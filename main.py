import shutil

from pdf2image import convert_from_path
import cv2
from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
import img2pdf
import os
import numpy as np
from pathlib import Path
from tkPDFViewer import tkPDFViewer as Pdf
import re
from tkinter import colorchooser
from threading import Thread
import eye

# Global Variables
p = ''
imgs = []
imgsq = []
imgs_after_edit = []
dirname = "path\\edited imgs\\"
dirname_pdf = "path\\pdfs\\"
dirname_mark = "path\\marked photos\\"
e1 = ''
rows = []
cols = []
imgs_after_mark_edit = []
marked_photos = []


def choose_color(choice):
    """
    This color works on the gui side to give the user to pick the color he wants for texts and background
    :param choice: used to make me understand what color I'm changing
    :return: the color picked form the gui
    """
    if choice == 1:
        # variable to store hexadecimal code of color
        RGB_color, hash_color = colorchooser.askcolor(title="Choose letters color")
        print(RGB_color)
    elif choice == 0:
        # variable to store hexadecimal code of color
        RGB_color, hash_color = colorchooser.askcolor(title="Choose background's color")
        print(RGB_color)
    return RGB_color


def threadFunc():
    """
    This function creates a new thread for the eye controlling base section in the app
    :return: None
    """
    th = Thread(target=eye.eyesController)
    th.start()


def file():
    """
    This function gives the user the ability to pick the pdf file from local files he has
    :return: None
    """
    global e1
    boo = False
    e1 = filedialog.askopenfilename(initialdir=os.getcwd(),
                                    title="select pdf file")

    pdf2img()
    print(e1)


def pdf2img():
    """
    This function convert the chosen pdf by the user and convert it to images to work with them
    :return: None
    """
    i = 0
    try:
        global rows, cols
        print(str(e1))
        p = Path((str(e1))).stem
        # print(p)
        images = convert_from_path(str(e1))
        for img in images:
            i += 1
            img2 = np.array(img)
            qwe = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
            imgsq.append(qwe)
    except Exception as e:
        print(e)
        # print(images)
        result = "NO pdf found"
        messagebox.showinfo("Result", result)

    else:
        result = "success"
        messagebox.showinfo("Result", result)
        return True


def read_img(dir_name_locally):
    """
    This function gets the paths of the for a specific path
    :param dir_name_locally: The specific path mentioned above
    :return: None
    """
    i = 0
    p = Path((str(e1))).stem
    print(len(imgsq))
    for fname in os.listdir(dir_name_locally):
        # print(fname)
        if not fname.endswith(".jpg"):
            continue
        path = os.path.join(dir_name_locally, fname)
        if os.path.isdir(path):
            continue
        if str(path).__contains__(p):
            print(str(path))
            imgs.append(path)
        # print(p)
    imgs.sort(key=lambda test_string: list(
        map(int, re.findall(r'\d+', test_string)))[0])


def eye_comfort():
    """
    This function modify the colors of the images (the pdf) to make them more comfortable to eyes
    :return: None
    """
    p = Path((str(e1))).stem
    print(p)
    for i, img in enumerate(imgsq):
        i += 1
        # img = cv2.imread(dirname + p + f' {i}.jpg')
        copy_img = img
        copy_img[:, :, 0] = (87 / 100) * copy_img[:, :, 0]
        img = keep_pics(img, copy_img)
        print(len(img))
        if len(img) == 0:
            img = copy_img
        cv2.imwrite(dirname + p + f' {i}.jpg', img)
    img2pdf1(dirname)


def change_color():
    """
    This function change the colors of letters and background based on his/her choice,
    without changing images colors inside pdf.
    :return: None
    """
    global rows, cols
    p = Path((str(e1))).stem
    letter_color = choose_color(1)
    background_color = choose_color(0)
    for i, img in enumerate(imgsq):
        i += 1
        gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        ret, threshold2 = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
        threshold2 = cv2.cvtColor(threshold2, cv2.COLOR_GRAY2RGB)
        threshold2[np.where((threshold2 == [255, 255, 255]).all(axis=2))] = letter_color
        threshold2[np.where((threshold2 == [0, 0, 0]).all(axis=2))] = background_color
        threshold2 = keep_pics(img, threshold2)
        rows, cols, _ = threshold2.shape
        print(len(img))
        if len(img) == 0:
            threshold2 = threshold2
        imgs_after_edit.append(threshold2)
    for i, im in enumerate(imgs_after_edit):
        i += 1
        cv2.imwrite(dirname + p + f' {i}.jpg', im)
    img2pdf1(dirname)


def delete_images(dir_name_locally):
    """
    Change all files inside specific directory
    :param dir_name_locally: the directory mentioned above
    :return: None
    """
    for filename in os.listdir(dir_name_locally):
        file_path = os.path.join(dir_name_locally, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))


def img2pdf1(dir_name_locally):
    """
    This function convert the images we edit to pdf file
    :param dir_name_locally: directory for the new pdf
    :return: None
    """
    global imgs,imgs_after_edit,imgs_after_mark_edit

    p = Path((str(e1))).stem

    read_img(dir_name_locally)
    print('in img2pdf func', dir_name_locally)
    try:
        if os.path.isdir(dirname_pdf):
            if os.path.isfile(dirname_pdf + p + "_after_edit.pdf"):
                os.remove(dirname_pdf + p + "_after_edit.pdf")
            with open(dirname_pdf + p + "_after_edit.pdf", "wb") as f:
                f.write(img2pdf.convert(imgs))

        else:
            os.mkdir(dirname_pdf)
            with open(dirname_pdf + p + "_after_edit.pdf", "wb") as f:
                f.write(img2pdf.convert(imgs))

        imgs = []
        imgs_after_edit = []
        imgs_after_mark_edit = []
        delete_images(dir_name_locally)
    except Exception as e:
        print(e)
        print(imgs)
        Result = "NO pdf found"
        messagebox.showinfo("Result", Result)

    else:
        Result = "success"
        messagebox.showinfo("Result", Result)


def mark(page_number):
    """
    This function put a mark on the page he wanted
    :param page_number: the number of the page
    :return: None
    """
    global marked_photos, imgs_after_mark_edit
    p = Path((str(e1))).stem
    for i, img in enumerate(imgsq):
        check_number = i + 1
        img = cv2.imread(dirname + p + f' {check_number}.jpg')
        print('112233445566')
        print(page_number)
        print(i + 1)

        if check_number == int(page_number):
            img2 = cv2.imread('path\\checkpoint.png')
            # img2 = cv2.resize(img2, None, 0.7, 0.7, cv2.INTER_LANCZOS4)
            img2 = cv2.resize(img2, (200, 200))
            print('Im in')
            rows, cols, channels = img2.shape
            roi = img[0:rows, 0:cols]
            img2gray = cv2.cvtColor(img2, cv2.COLOR_RGB2GRAY)
            ret, mask = cv2.threshold(img2gray, 10, 255, cv2.THRESH_BINARY)
            mask_inv = cv2.bitwise_not(mask)
            img_bg = cv2.bitwise_and(roi, roi, mask=mask_inv)
            img2_fg = cv2.bitwise_and(img2, img2, mask=mask)
            # Put logo in ROI and modify the main image
            dst = cv2.add(img_bg, img2_fg)
            img[0:rows, 0:cols] = dst
            # cv2.imshow('after effect', img)
            imgs_after_mark_edit.append(img)
        else:
            imgs_after_mark_edit.append(img)
    print("the len of the edited array of iimgs")
    print(len(imgs_after_mark_edit))
    for i, im in enumerate(imgs_after_mark_edit):
        i += 1
        marked_photos.append(im)
        cv2.imwrite(dirname_mark + p + f'marked {i}.jpg', im)
    imgs_after_mark_edit = []
    img2pdf1(dirname_mark)


def view_pdf(boo):
    """
    This function work on viewing the pdf before editing or after
    :param boo: boolean variable to understand what this function will view
    :return: None
    """
    global rows, cols, e1
    try:
        new = Toplevel(master=master)
        new.geometry(f"{cols}x{rows}")
        new.title("New Window")
        page_number = Entry(new, font=("default", 22))
        page_number.pack()
        Button(new, text="put a mark", command=lambda: mark(page_number.get())).pack()
        Button(new, text="Eye controlling", command=lambda: threadFunc()).pack()
        v1 = Pdf.ShowPdf()
        v1.img_object_li.clear()
        p = Path((str(e1))).stem
        if boo == 0:

            filename = str(e1)
            v2 = v1.pdf_view(master=new, pdf_location=open(filename), width=rows, height=cols)
            v2.pack(pady=(0, 0))

        elif boo == 1:

            filename = dirname_pdf + p + "_after_edit.pdf"
            v2 = v1.pdf_view(master=new, pdf_location=open(filename), width=rows, height=rows)
            v2.pack(pady=(0, 0))

    except Exception as e:
        print(e)


def modify_pics(cut_image, y, newy, x, newx, original_photo, colored_phto):
    """
    This function works on putting the image of pdf in its exact place in the new modified pdf
    :param cut_image: the image of the pdf we want to put
    :param y, newy, x, newx:the dimension of the image
    :param original_photo: the original page of the pdf that has this image without modifying
    :param colored_phto: the colored page we want to put the image in
    :return: the colored page modified with the original image
    """
    print('Im in')
    roi = original_photo[y:newy, x:newx]
    img2gray = cv2.cvtColor(cut_image, cv2.COLOR_RGB2GRAY)
    ret, mask = cv2.threshold(img2gray, 10, 255, cv2.THRESH_BINARY)
    mask_inv = cv2.bitwise_not(mask)
    img_bg = cv2.bitwise_and(roi, roi, mask=mask_inv)
    img2_fg = cv2.bitwise_and(cut_image, cut_image, mask=mask)
    # Put logo in ROI and modify the main image
    dst = cv2.add(img_bg, img2_fg)
    colored_phto[y:newy, x:newx] = dst
    # cv2.imshow('after effect', img)
    return colored_phto


def keep_pics(original_img, colored_img):
    """
    This function is getting the images of the pdf file to put them without modifying in the colored pdf
    :param original_img: the original page
    :param colored_img: the colored page
    :return: colored page with the original images in it
    """
    iiii = colored_img
    images = []
    indicies = []

    img = original_img
    cannyFilter = cv2.Canny(img, threshold1=150, threshold2=300)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, threshimg = cv2.threshold(gray, 0, 255, cv2.THRESH_OTSU + cv2.THRESH_BINARY_INV)
    # defying kernel size and structure shape.
    rect_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (18, 18))
    kernal = np.ones((18, 18), np.uint8)
    #
    openning = cv2.morphologyEx(threshimg, cv2.MORPH_OPEN, kernal)
    op1 = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernal)
    # Applying dilation on the threshold image
    dilation = cv2.dilate(cannyFilter, rect_kernel, iterations=3)
    # getting contours
    img_contours, hierarchy = cv2.findContours(cannyFilter, cv2.RETR_TREE,
                                               cv2.CHAIN_APPROX_NONE)
    # Loop over contours and crop and extract the text file
    ro, co = cannyFilter.shape
    area_threshold1 = ro * co / 2
    area_threshold2 = ro * co / 25
    for cnt in img_contours:
        x, y, w, h = cv2.boundingRect(cnt)
        if w * h < area_threshold1 and w * h > area_threshold2:
            x, y, w, h = cv2.boundingRect(cnt)
            cropped_img = img[y:y + h, x:x + w]
            iiii = modify_pics(cropped_img, y, y + h, x, x + w, img, colored_img)
            newx = x + w
            newy = y + h
            indicies.append((y, newy, x, newx))
            images.append(cropped_img)
    if len(iiii) == 0:
        return colored_img
    return iiii


master = Tk()
master.geometry("600x300+150+150")
master.title("PDF viewer ")
Label(master, text="File Location").pack()

Button(master, text="Choose a PDF file", command=lambda: file()).pack()

Button(master, text="Change color", command=lambda: change_color()).pack()

Button(master, text="Show pdf file before editing", command=lambda: view_pdf(0)).pack()

Button(master, text="Show pdf file after editing", command=lambda: view_pdf(1)).pack()

Button(master, text="Change to eye comfort", command=eye_comfort).pack()

mainloop()
